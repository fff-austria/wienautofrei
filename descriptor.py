import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt
img1 = cv.imread('auto1.jpg')
img2 = cv.imread('auto2.jpg')
img3 = cv.imread('wienautos.jpg')

# Initiate ORB detector
orb = cv.ORB_create()
# find the keypoints with ORB
kp1 = orb.detect(img1,None)
kp2 = orb.detect(img2,None)
kp3 = orb.detect(img3,None)


# compute the descriptors with ORB
kp1, des1 = orb.compute(img1, kp1)
kp2, des2 = orb.compute(img2, kp2)
kp3, des3 = orb.compute(img3, kp3)




print(len(kp1))
print(len(des1))
print()
print(len(kp2))
print(len(des2))
print()
n1 = np.linalg.norm(des1 - des2, 2)
n2 = np.linalg.norm(des1 - des3, 2)

print(n1)
print(n2)

# draw only keypoints location,not size and orientation

#img2 = cv.drawKeypoints(img, kp, None, color=(0,255,0), flags=0)
#plt.imshow(img2), plt.show()